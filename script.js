class Employee {
  constructor (name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get name () {
    return this._name;
  }
  set name (newName) {
    newName = newName.trim();
    if (newName === '') {
       alert ('Enter an Employee name');
    }
    this._name = newName;
  }
  get age () {
    return this._age;
  }
  set age (newAge) {
    !isNaN(newAge) && newAge >= 18 ? this._age = newAge : alert('Enter correct age');
  }
  get salary () {
    return this._salary;
  }
  set salary (newSalary) {
    !isNaN(newSalary) && newSalary > 0 ? this._salary = newSalary : alert('Enter correct salary')
    
  }

}

class Programmer extends Employee {
  constructor (name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get lang () {
    return this._lang;
  }
  set lang (newLang) {
    if (newLang === '') {
      alert ('Enter correct language');
   }
    this._lang = newLang;
  }
  
  get salary () {
    return super.salary * 3;
  }
  set salary (newSalary) {
    super.salary = newSalary;
  }
}

let franko = new Programmer ("Ivan", 33 , 1000, "English, German");
let shevchenko = new Programmer ("Taras", 41, 800, "Italian");
let ukrainka = new Programmer ("Lesia", 24, 950, "French");

console.log(franko);
console.log(shevchenko);
console.log(ukrainka);



// Варіант без set/get
// class Employee {
//   constructor (name, age, salary) {
//     this.name = name;
//     this.age = age;
//     this.salary = salary;
//   }
// }

// class Programmer extends Employee {
//   constructor (name, age, salary, lang) {
//     super(name, age, salary);
//     this.lang = lang;
//   }
 
//   set salary (salary) {
//     this._salary = salary * 3;
//   }
// }

// let Franko = new Programmer ("Ivan", "33", "1000", "English, German");
// let Shevchenko = new Programmer ("Taras", "28", "800", "Italian");
// let Ukrainka = new Programmer ("Lesia", "24", "950", "French");

// console.log(Franko);
// console.log(Shevchenko);
// console.log(Ukrainka);
